# web-cloud

## 整体说明：
基于springmvc+bootstrap搭建的web开发平台。

## 环境：
> * jdk1.7
> * mysql
> * 安装maven
> * Apache Tomcat7
> * 安装nodejs
> * 通过bower进行js模块管理
> * 安装git（代码版本管理）
> * idea 开发平台

## 技术：
> * spring-mvc
> * bootstrap（admin+LTE）
> * shiro做登陆权限验证
> * spring session 集成Redis
> *

## 问题
> * Thrift 做RPC调用
