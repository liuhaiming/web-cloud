/**
 * Created by liuhaiming on 2016-05-01.
 */
var login = {
    refreshValidateCode: function (obj) {
        obj.src = "${root}/img/validate_code?" + Math.random();
    },

    init: function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        validator.init();

        var form = $("#loginForm");

        form.validate({
            submitHandler: function (form) {
                $('[type="submit"]').button('loading');
                form.submit();
            },
            errorPlacement: function (error, element) {
                element.parent().parent('div').append(error);
            },
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                validateCode: {
                    required: true,
                    rangelength: [4, 4]
                }
            },
            messages: {
                username: {
                    required: "请输入用户名"
                },
                password: {
                    required: "请输入密码"
                },
                validateCode: {
                    required: "请输入验证码",
                    rangelength: "请输入4位验证码"
                }
            }
        });
    }
}