function initComAjax(options) {
    var settings = {
        url         : options.url,
        targetId    : options.targetId == null ? null : "#" + options.targetId,
        data        : options.data == null ? {} : options.data,
        async       : options.async == null ? true : options.async,
        dataType    : options.dataType == null ? "json" : options.dataType,
        successFunc : options.successFunc,
        errorFunc   : options.errorFunc,
        actionName  : options.actionName == null ? "ajax处理" : options.actionName
    };

    var comAjax = new ComAjax(settings);
    return comAjax;
}

function ComAjax(optinos) {
    this.settings = optinos;

    this.sendGet = function() {
        this.sendAjax("GET");
    }

    this.sendPost = function() {
        this.sendAjax("POST");
    }

    this.sendAjax = function(sendType) {
        var _self = this;

        if (_self.settings.targetId != null) {
            Metronic.blockUI({
                target       :_self.settings.targetId,
                overlayColor : 'none',
                animate      : true
            });
        }

        $.ajax({
            url      : _self.settings.url,
            data     : _self.settings.data,
            dataType : "json",
            async    : true,
            type     : sendType,
            success  : function (result) {
                if (_self.settings.targetId != null) {
                    Metronic.unblockUI(_self.settings.targetId);
                }

                var resultType = eval(result).result;
                if (resultType == "success") {
                    if ( _self.settings.successFunc != null ) {
                        _self.settings.successFunc(result);
                    }
                } else {
                    alert( _self.settings.actionName + "失败!" );
                }
            },
            error    : function (XMLHttpRequest, textStatus, errorThrown) {
                if (_self.settings.targetId != null) {
                    Metronic.unblockUI(_self.settings.targetId);
                }

                alert(_self.settings.actionName + "失败!");

                if ( _self.settings.errorFunc != null ) {
                    _self.settings.errorFunc(XMLHttpRequest, textStatus, errorThrown);
                }
            }
        });
    }
}