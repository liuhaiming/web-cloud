/**
 * Created by liuhm on 2015/8/15
 */

var PubTree = function() {
    var treeDefaultSet = function(options) {
        var treeId = options.treeId;
        var defOpenAll = options.defOpenAll == null ? false : options.defOpenAll;
        var multiple = options.multiple == null ? false : options.multiple;
        var rootCanSelect = options.rootCanSelect == null ? true : options.rootCanSelect;
        var loadedFunc = options.loadedTree;
        var chgNodeFunc = options.changedNode;
        var dblClickNodeFunc = options.dblClickNode;
        var hasCheckBox = options.hasCheckBox == null ? false : options.hasCheckBox;
        var plugins = hasCheckBox ? ["type", "checkbox"] : ["type"];

        var tree = $("#" + treeId);
        var pubTree = tree.jstree({
            "core": {
                "themes" : {
                    "responsive": false
                },
                "data" : {
                    "url" : options.url,
                    "data" : function (node) {
                    }
                },
                "multiple":multiple,
                "strings" : {
                    "loading..." : "加载中..."
                },
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder"
                },
                "file": {
                    "icon": "fa fa-file"
                }
            },
            "plugins": plugins
        });

        pubTree.currSelectNode = null;

        pubTree.on('loaded.jstree', function(e, data){
            $(".portlet-body-scroller").css("overflow","auto");
            $(".portlet-body-scroller").css("overflow-x","auto");
            $(".portlet-body-scroller").css("overflow-y","hidden");
            $(".portlet-body-scroller").css("padding-bottom","15px");

            var instance = data.instance;
            if ( defOpenAll ) {
                instance.open_all(0);
            }

            var root = instance.get_node(e.target.firstChild.firstChild.lastChild);

            if ( !rootCanSelect ) {
                var firstNode = instance.get_children_dom(root).eq(0);
                instance.select_node(firstNode);
                instance.disable_node(root);
            } else {
                if (!hasCheckBox) {
                    instance.select_node(root);
                }
            }

            if(loadedFunc != null) {
                loadedFunc(e, data);
            }
        })

        pubTree.bind('changed.jstree', function(e, data) {
            if ( data == null || data.node == null ) {
                return;
            } else if ( !rootCanSelect && data.node.id == "root" ) {
                return;
            } else if ( pubTree.currSelectNode != null && (pubTree.currSelectNode.id == data.node.id) ) {
                return;
            }

            pubTree.currSelectNode = data.node;
            if (chgNodeFunc != null) {
                chgNodeFunc(e, data);
            }
        });

        pubTree.bind('dblclick.jstree', function(e, data) {
            if ( dblClickNodeFunc ) {
                dblClickNodeFunc(e, data);
            }
        });

        return pubTree;
    };

    return {
        init : function(options) {
            return treeDefaultSet(options);
        }
    }
}();

