var jzSelect2 = {
    settings : {
        url:"",
        dataType:"json",
        name:"",
        value:"",
        successFunc:function(){}
    },
    sendAjax : function() {
        initJzAjax({
            url:jzSelect2.settings.url,
            dataType:jzSelect2.settings.dataType,
            actionName:"加载下拉列表[name='" + jzSelect2.settings.name + "']",
            successFunc:function(result){
                var sel2 = $("select[name=" + jzSelect2.settings.name + "]").empty();
                var option = "";
                if ( eval(result).error_code == "200" ) {
                    var items = eval(result).data;
                    $(items).each(function(){
                        option += "<option value='" + this.id + "'>" + this.code + " " + this.name + "</option>";
                    });

                }

                sel2.html(option).select2({
                    minimumResultsForSearch: -1,
                    formatNoMatches:function(){
                        return "未找到匹配的角色";
                    }
                });

                sel2.select2("val", jzSelect2.settings.value);

                if ( jzSelect2.settings.successFunc != null ) {
                    jzSelect2.settings.successFunc();
                }
            },
        }).sendGet();

    },
    init : function(options) {
        jzSelect2.settings.url = options.url;
        jzSelect2.settings.dataType = options.dataType == jzSelect2 ? jzSelect2.settings.dataType : options.dataType;
        jzSelect2.settings.name = options.name == null ? jzSelect2.settings.name : options.name;
        jzSelect2.settings.value = options.value == null ? jzSelect2.settings.value : options.value;
        jzSelect2.settings.successFunc = options.successFunc == null ? jzSelect2.settings.successFunc : options.successFunc;

        if ( jzSelect2.settings.url != null ) {
            this.sendAjax();
        } else {
            $("select[name=" + jzSelect2.settings.name + "]").empty();
        }
    }
}