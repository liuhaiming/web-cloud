
//------处理   下拉从点击变成鼠标移入
;(function($, window, undefined) {
    // outside the scope of the jQuery plugin to
    // keep track of all dropdowns
    var $allDropdowns = $();

    // if instantlyCloseOthers is true, then it will instantly
    // shut other nav items when a new one is hovered over
    $.fn.dropdownHover = function(options) {
        // the element we really care about
        // is the dropdown-toggle's parent
        $allDropdowns = $allDropdowns.add(this.parent());

        return this.each(function() {
            var $this = $(this).parent(),
                defaults = {
                    delay: 500,
                    instantlyCloseOthers: true
                },
                data = {
                    delay: $(this).data('delay'),
                    instantlyCloseOthers: $(this).data('close-others')
                },
                options = $.extend(true, {}, defaults, options, data),
                timeout;

            $this.hover(function() {
                if(options.instantlyCloseOthers === true)
                    $allDropdowns.removeClass('open');

                window.clearTimeout(timeout);
                $(this).addClass('open');
            }, function() {
                timeout = window.setTimeout(function() {
                    $this.removeClass('open');
                }, options.delay);
            });
        });
    };

    $('[data-hover="dropdown"]').dropdownHover();
})(jQuery, this);
//------下拉功能结束


$(function(){
	pub.handleScrollers();
    pub.initDatePickerContainerOffset();
});

//处理左侧菜单，展开模式
(function($, window, undefined) {
    $("#side-menu").find("a.active").each(function(index){
        $(this).parents('ul').addClass("in");
    });


})(jQuery,this);


var pub = {};
pub.handleScrollers = function() {
	pub.initSlimScroll('.scroller');
}

pub.initDatePickerContainerOffset = function () {
    $("#datePickerContainer").offset({top:0, left:0});

    $('.date-picker').datepicker({
        language: 'zh-CN',
        autoclose: true,
        format: "yyyy-mm-dd",
        container:"#datePickerContainer"
    });
}

pub.initSlimScroll = function(el) {
	$(el).each(function() {
		if ($(this).attr("data-initialized")) {
			return; // exit
		}

		var height;

		if ($(this).attr("data-height")) {
			height = $(this).attr("data-height");
		} else {
			height = $(this).css('height');
		}

		$(this).slimScroll({
			allowPageScroll: true, // allow page scroll when the element scroll is ended
			size: '7px',
			color: ($(this).attr("data-handle-color") ? $(this).attr("data-handle-color") : '#bbb'),
			wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
			railColor: ($(this).attr("data-rail-color") ? $(this).attr("data-rail-color") : '#eaeaea'),
			position: 'right',
			height: height,
			alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
			railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
			disableFadeOut: true
		});

		$(this).attr("data-initialized", "1");
	});
}

pub.isNull = function(obj) {
    return obj == null || !obj;
}
