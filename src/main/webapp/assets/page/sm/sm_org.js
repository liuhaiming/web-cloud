var smOrg = {};
smOrg.action = null;
smOrg.selectId = null;
smOrg.selTs = null;
smOrg.tree;
smOrg.userType = null;
smOrg.rootCanSelect = null;

smOrg.init = function(userType) {
    smOrg.userType = userType;
    if ( smOrg.userType == 0 ) {
        smOrg.rootCanSelect = true;
    } else {
        smOrg.rootCanSelect = false;
    }

    smOrg.initOrgTree();
    smOrg.initEvent();
}

smOrg.initEvent = function() {
    $("#edit_order").click(smOrg.edit);
    $("#del_order").click(smOrg.del);
    $("#enable_order").click(smOrg.enable);
    $("#disable_order").click(smOrg.disable);

    //$("#reload_order").click(smOrg.reloadOrgTree);
    //$("#reload_sel_order").click(smOrg.reloadSelOrgInfo);

    $("#save_order").click(smOrg.save);

}

smOrg.initOrgTree = function() {
    smOrg.tree = PubTree.init({
        "treeId": "orgTree",
        "url": rootPath + "/sm/sm_org_tree",
        "multiple":false,
        "defOpenAll":true,
        "rootCanSelect":smOrg.rootCanSelect,
        "loadedTree":function(e, data) {

        },
        "changedNode":function(e, data) {
            smOrg.loadOrgData( data.node.id );
        }

    });

    var form = $("#orgDetailForm");
    var validate = form.validate({
        submitHandler: function(form) {
            initComAjax({
                url: $(form).attr("action"),
                data: $(form).serializeArray(),
                actionName:"组织保存",
                targetId:"smOrgSection",
                successFunc:function(result){
                    $('#orgDialog').modal('hide');
                    alert("保存成功!");
                    window.location.reload();
                },
            }).sendPost();

        },
        rules:{
            code:{
                required:true
            },
            name:{
                required:true
            },
        },
        messages:{
            code:{
                required:"必填",
                maxlength:"请不要超过20位"
            },
            name:{
                required:"必填",
                maxlength:"请不要超过50位"
            },
            info:{
                maxlength:"请不要超过400位"
            }
        }
    });
    var dialog = $("#orgDialog");

    dialog.on("hidden.bs.modal", function () {
        $(this).removeData("bs.modal");
        form.find("input").val("");
        validate.resetForm();
        form.find('.form-group').removeClass('has-error');
        form.find("#edit-method").empty();
        smOrg.action = null;
    });
    dialog.on("show.bs.modal", function () {

        if(smOrg.action == "edit") {
            initComAjax({
                url: "sm/sm_org/" + smOrg.selectId,
                data: {},
                actionName:"组织信息加载",
                targetId:"smOrgSection",
                successFunc:function(result){
                    var result = eval(data).result;
                    if (result == "success") {
                        var org = eval(data).data;
                        form.find("[name = 'id']").val(org.id);
                        form.find("[name = 'ts']").val(org.ts);
                        form.find("[name = 'code']").val(org.code);
                        form.find("[name = 'name']").val(org.name);
                        form.find("[name = 'info']").val(org.info);
                        form.find("[name = 'memo']").val(org.memo);
                        form.find("[name = 'parentId']").val(org.parentId);
                        form.find("[name = 'status']").select2("val", org.status);
                        form.find("#edit-method").html("<input type='hidden' name='_method' value='PUT'></div>");
                    }
                },
                errorFunc : function (XMLHttpRequest, textStatus, errorThrown) {
                    $('#orgDialog').modal('hide');
                }
            }).sendGet();

        } else {
            form.find("[name = 'id']").val(null);
            form.find("[name = 'ts']").val(null);
            form.find("[name = 'code']").val(null);
            form.find("[name = 'name']").val(null);
            form.find("[name = 'info']").val(null);
            form.find("[name = 'memo']").val(null);
            form.find("[name = 'status']").select2("val", 0);

            form.find("[name = 'parentId']").val(smOrg.selectId);
        }
    });
}

smOrg.loadOrgData = function (id) {
    if ( id == null || id == "root" ) {
        $("#dispCode").val( null );
        $("#dispName").val( null );
        $("#dispInfo").val( null );
        $("#dispStatus").val(null);
        $("#dispMemo").val( null );

        smOrg.selectId = null;
        smOrg.selTs = null;
        return ;
    }

    initComAjax({
        url: "sm/sm_org/" + id,
        data: {},
        actionName:"组织信息加载",
        targetId:"smOrgSection",
        successFunc:function(result){
            var org = result.data;
            $("#selNodeId").val(id);
            smOrg.initEditData(org);
        },
    }).sendGet();
}

smOrg.initEditData = function (org) {
    $("#dispCode").val( org.code );
    $("#dispName").val( org.name );
    $("#dispInfo").val( org.info );
    //$("#dispStatus").select2("val", org.status);
    if (org.status == 0) {
        $("#dispStatus").val("启用");
    }else {
        $("#dispStatus").val("停用");
    }
    $("#dispMemo").val( org.memo );

    smOrg.selectId = org.id;
    smOrg.selTs = org.ts;
}

smOrg.save = function () {
    $("#orgDetailForm").submit();
}

smOrg.del = function () {
    if( smOrg.selectId == null ) {
        alert("请选择组织！");
    } else if (smOrg.tree.currSelectNode.children.length > 0) {
        alert("非末级组织不允许删除!");
    } else {
        if(window.confirm("确定要删除选择条数据吗？")){
            var form = $("#orgDelForm");
            initComAjax({
                url: $(form).attr("action"),
                data: {id:smOrg.selectId, ts:smOrg.selTs, _method:"DELETE"},
                actionName:"组织删除",
                targetId:"smOrgSection",
                successFunc:function(result){
                    alert("删除成功");
                    window.location.reload();
                },
            }).sendPost();

        }
    }
}

smOrg.edit = function () {
    if (smOrg.tree.currSelectNode != null && tree.currSelectNode.id == 'root') {
        alert("组织根节点不允许编辑！");
    } else if( smOrg.selectId == null ) {
        alert("请选择组织！");
    }else {
        smOrg.action = "edit";
        $('#orgDialog').modal('show');
    }
}

smOrg.updateStatus = function (status) {

    var statusTxt;

    if(status == 0) {
        statusTxt = "启用";
    }else{
        statusTxt = "禁用";
    }

    if (smOrg.tree.currSelectNode != null && smOrg.tree.currSelectNode.id == 'root') {
        alert("组织根节点不允许编辑！");
    } else if( smOrg.selectId == null ) {
        alert("请选择组织！");
    }else {
        if(window.confirm("确定要"+ statusTxt + "选中组织吗？")){
            var form = $("#orgUpdateStatusForm");
            initComAjax({
                url: $(form).attr("action") + "/" + status,
                data: {id:smOrg.tree.currSelectNode.id, ts:smOrg.tree.currSelectNode.ts, _method:"PUT"},
                actionName:"组织" + statusTxt,
                targetId:"smOrgSection",
                successFunc:function(result){
                    alert(statusTxt + "成功");
                    window.location.reload();
                },
            }).sendPost();

        }
    }
}

smOrg.enable = function () {
    smOrg.updateStatus(0);
}

smOrg.disable = function () {
    smOrg.updateStatus(1);
}

smOrg.reloadOrgTree = function () {
    smOrg.tree.jstree("refresh",false,0);
}

smOrg.reloadSelOrgInfo = function () {
    if ( smOrg.tree.currSelectNode != null ) {
        smOrg.loadOrgData(smOrg.tree.currSelectNode.id);
    }
}