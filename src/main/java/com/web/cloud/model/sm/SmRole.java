package com.web.cloud.model.sm;

import com.web.cloud.annotation.AnnotationTableSearchColumn;
import com.web.cloud.constances.DBConsts;
import com.web.cloud.model.pub.IOrgBaseModel;

import javax.persistence.*;

@Table(name = "sm_role")
public class SmRole implements IOrgBaseModel {
    @Id
    private String id;

    @OrderBy
    @AnnotationTableSearchColumn
    private String code;

    @Column(name = "org_id")
    private String orgId;

    @Transient
    private SmOrg org;

    @Column(name = "is_admin")
    private String isAdmin;

    @Column(length = 50)
    @AnnotationTableSearchColumn
    private String name;

    private Short status;

    @Column(length = 400)
    private String memo;

    private Long ts;

    private Short dr = DBConsts.DR_NORMAL;;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    @Override
    public String getOrgId() {
        return orgId;
    }

    @Override
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public SmOrg getOrg() {
        return org;
    }

    public void setOrg(SmOrg org) {
        this.org = org;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    @Override
    public Long getTs() {
        return ts;
    }

    @Override
    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Short getDr() {
        return dr;
    }

    public void setDr(Short dr) {
        this.dr = dr;
    }
}