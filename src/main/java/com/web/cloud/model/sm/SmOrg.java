package com.web.cloud.model.sm;


import com.web.cloud.constances.DBConsts;
import com.web.cloud.model.pub.IBaseModel;
import com.web.cloud.model.pub.tree.ITreeNode;

import javax.persistence.*;

@Table(name = "sm_org")
public class SmOrg implements IBaseModel, ITreeNode {
    @Id
    private String id;

    @OrderBy
    private String code;

    private String incode;

    @Column(length = 50)
    private String name;

    private String info;

    private Short status;

    @Column(length = 400)
    private String memo;

    @Column(name="parent_id")
    private String parentId;

    private Short sq;

    private String appId;

    private String appSecret;

    private Long ts;

    private Short dr = DBConsts.DR_NORMAL;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getIncode() {
        return incode;
    }

    public void setIncode(String incode) {
        this.incode = incode == null ? null : incode.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    @Override
    public String getNodeId() {
        return id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    public Short getSq() {
        return sq;
    }

    public void setSq(Short sq) {
        this.sq = sq;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    @Override
    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Short getDr() {
        return dr;
    }

    public void setDr(Short dr) {
        this.dr = dr;
    }
}