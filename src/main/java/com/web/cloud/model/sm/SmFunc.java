package com.web.cloud.model.sm;

import com.web.cloud.model.pub.IBaseModel;
import com.web.cloud.model.pub.tree.ITreeNode;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * 功能节点数据模型
 * 
 * @author liuhm
 * @since 2015年12月20日
 **/
@Table(name="sm_func")
public class SmFunc implements IBaseModel, ITreeNode {
    @Id
    private String id;

    private String code;

    private String name;

    private String href;

    private String incode;

    private String icon;

    private String parentId;

    private Short status;

    private String memo;

    private Short nodeType;

    private Long ts;

    private Short dr;

    private Short sq;

    @Transient
    private List<SmFunc> childrens = null;

    public SmFunc() {

    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getIncode() {
        return incode;
    }

    public void setIncode(String incode) {
        this.incode = incode;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String getNodeId() {
        return getId();
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    @Override
    public Long getTs() {
        return ts;
    }

    @Override
    public void setTs(Long ts) {
        this.ts = ts;
    }

    @Override
    public Short getDr() {
        return dr;
    }

    @Override
    public void setDr(Short dr) {
        this.dr = dr;
    }

    public Short getSq() {
        return sq;
    }

    public void setSq(Short sq) {
        this.sq = sq;
    }

    public Short getNodeType() {
        return nodeType;
    }

    public void setNodeType(Short nodeType) {
        this.nodeType = nodeType;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public List<SmFunc> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<SmFunc> childrens) {
        this.childrens = childrens;
    }
}