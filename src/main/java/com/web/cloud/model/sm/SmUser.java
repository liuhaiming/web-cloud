package com.web.cloud.model.sm;

import com.web.cloud.annotation.AnnotationTableSearchColumn;
import com.web.cloud.constances.DBConsts;
import com.web.cloud.model.pub.IBaseModel;
import com.web.cloud.model.pub.IOrgBaseModel;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * 用户数据模型
 * 
 * @author liuhm
 * @since 2014年7月5日 下午12:07:20
 **/
@Table(name="sm_user")
public class SmUser implements IOrgBaseModel {
    public static final Short ROLE_FIND_TYPE_MAIN = 0;
    public static final Short ROLE_FIND_TYPE_AUXILIARY = 1;
    public static final Short ROLE_FIND_TYPE_ALL = 2;

    @Id
    @Column(length = 20)
    private String id;

    @AnnotationTableSearchColumn
    private String code;

    @AnnotationTableSearchColumn
    private String name;

    private String password;

    @Column(name = "busi_org_id")
//    @TableSearchColumn(column = "name", joinTable = "sm_org")
    private String busiOrgId;

    @Transient
    private SmOrg busiOrg;

    @Column(name = "org_id")
    private String orgId;

    @Column(name = "role_id")
//    @TableSearchColumn(column = "name", joinTable = "sm_role")
    private String roleId;

    @Transient
    private SmRole role;

    private Short sex;

    @AnnotationTableSearchColumn
    private String phone;

    private Long ts;

    private Short dr = DBConsts.DR_NORMAL;

    @Column(name = "header_img")
    private String headerImg;

    @Column(name = "id_card")
    private String idCard;

    private Short status;

    private String memo;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getHeaderImg() {
        return headerImg;
    }

    public void setHeaderImg(String headerImg) {
        this.headerImg = headerImg;
    }

    public String getBusiOrgId() {
        return busiOrgId;
    }

    public void setBusiOrgId(String busiOrgId) {
        this.busiOrgId = busiOrgId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public SmOrg getBusiOrg() {
        return busiOrg;
    }

    public void setBusiOrg(SmOrg busiOrg) {
        this.busiOrg = busiOrg;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public SmRole getRole() {
        return role;
    }

    public void setRole(SmRole role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Override
    public Long getTs() {
        return ts;
    }

    @Override
    public void setTs(Long ts) {
        this.ts = ts;
    }

    @Override
    public Short getDr() {
        return dr;
    }

    @Override
    public void setDr(Short dr) {
        this.dr = dr;
    }

    public static void main(String[] args) {
        System.out.println(new Date().getTime());
    }
}
