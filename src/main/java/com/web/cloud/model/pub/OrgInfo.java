package com.web.cloud.model.pub;

/**
 * 组织信息封装类
 * Created by hubo on 16/4/1
 */
public class OrgInfo {

    private String id;

    private String code;

    private String incode;

    private String name;

    public OrgInfo( String id, String code, String incode, String name) {
        this.id = id;
        this.code = code;
        this.incode = incode;
        this.name = name;
    }

    public OrgInfo() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIncode() {
        return incode;
    }

    public void setIncode(String incode) {
        this.incode = incode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
