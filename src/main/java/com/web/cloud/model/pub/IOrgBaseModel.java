package com.web.cloud.model.pub;

/**
 * Created by liuhaiming on 2016-05-29.
 */
public interface IOrgBaseModel extends IBaseModel {
    String getOrgId();

    void setOrgId(String var1);
}