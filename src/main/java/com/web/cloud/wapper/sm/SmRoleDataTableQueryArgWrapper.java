package com.web.cloud.wapper.sm;


import com.web.cloud.wapper.AbstractDataTableQueryArgWapper;

import java.util.Arrays;
import java.util.List;

public class SmRoleDataTableQueryArgWrapper extends AbstractDataTableQueryArgWapper {

    public SmRoleDataTableQueryArgWrapper(){

    }

    public SmRoleDataTableQueryArgWrapper(int draw, int start, int length, String searchText, int orderIndex, String orderType) {
        super(draw, start, length, searchText, orderIndex, orderType);
    }

    @Override
    public List<String> getColNameList() {
        return Arrays.asList("", "id", "code", "name", "status", "memo");
    }
}
