package com.web.cloud.utils;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Calendar;

/**
 * 密码加密算法，用于用户密码修改和密码校验用
 * Created by liuhm on 2015/12/8.
 */
public class EncryptUtil {
    /**
     * 密码加密，目前采取：登录名加密+密码加密，然后整体再次加密
     * @param
     * @return
     */
    public static String encryptForUser(String loginName, String password) {
        long t1 = Calendar.getInstance().getTimeInMillis();

        String encryptName = DigestUtils.sha512Hex(loginName);
        String encryptPsw = DigestUtils.sha512Hex(password);
        String encrypt = DigestUtils.sha512Hex(encryptName + encryptPsw);

        long t2 = Calendar.getInstance().getTimeInMillis();
        System.out.println( "password encrypt cost times:" + (t2 - t1) );

        return encrypt;
    }
}
