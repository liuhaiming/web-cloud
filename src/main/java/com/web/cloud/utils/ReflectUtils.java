package com.web.cloud.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by liuhaiming on 2016-05-29.
 */
public class ReflectUtils {
    public ReflectUtils() {
    }

    public static <T> Class<T> findParameterizedType(Class<?> clazz) {
        Type parameterizedType = clazz.getGenericSuperclass();
        if(!(parameterizedType instanceof ParameterizedType)) {
            parameterizedType = clazz.getSuperclass().getGenericSuperclass();
        }

        if(!(parameterizedType instanceof ParameterizedType)) {
            return null;
        } else {
            Type[] actualTypeArguments = ((ParameterizedType)parameterizedType).getActualTypeArguments();
            return actualTypeArguments != null && actualTypeArguments.length != 0?(Class)actualTypeArguments[0]:null;
        }
    }
}

