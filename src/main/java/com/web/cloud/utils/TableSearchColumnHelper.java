package com.web.cloud.utils;

import com.web.cloud.annotation.AnnotationTableSearchColumn;
import org.apache.commons.lang3.StringUtils;
import tk.mybatis.mapper.entity.EntityField;
import tk.mybatis.mapper.mapperhelper.EntityHelper;
import tk.mybatis.mapper.mapperhelper.FieldHelper;
import tk.mybatis.mapper.util.StringUtil;

import javax.persistence.Entity;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Created by hubo on 2015/8/27
 */
public class TableSearchColumnHelper {
    private static final Map<Class<?>, List<TableSearchColumnHelper.TableSearchColumn>> searchColumnMap = new HashMap();

    public TableSearchColumnHelper() {
    }

    public static List<TableSearchColumnHelper.TableSearchColumn> getTableSearchColumns(Class<?> entityClass) {
        if(!searchColumnMap.containsKey(entityClass)) {
            Map var1 = searchColumnMap;
            synchronized(searchColumnMap) {
                if(!searchColumnMap.containsKey(entityClass)) {
                    searchColumnMap.put(entityClass, new ArrayList());
                    List fieldList = FieldHelper.getFields(entityClass);
                    Iterator var3 = fieldList.iterator();

                    while(var3.hasNext()) {
                        EntityField field = (EntityField)var3.next();
                        if(field.isAnnotationPresent(AnnotationTableSearchColumn.class)) {
                            AnnotationTableSearchColumn column = field.getAnnotation(AnnotationTableSearchColumn.class);
                            String columnName = column.column();
                            if(StringUtils.isEmpty(StringUtils.trim(columnName))) {
                                columnName = StringUtil.camelhumpToUnderline(field.getName());
                            }

                            TableSearchColumnHelper.TableSearchColumn c = new TableSearchColumnHelper.TableSearchColumn();
                            c.setColumn(columnName);
                            if(StringUtils.isNotEmpty(column.joinTable())) {
                                c.setJoinTable(column.joinTable());
                                c.setJoinId(StringUtil.camelhumpToUnderline(field.getName()));
                            }

                            ((List)searchColumnMap.get(entityClass)).add(c);
                        }
                    }
                }
            }
        }

        return (List)searchColumnMap.get(entityClass);
    }

    public static class TableSearchColumn {
        private String column;
        private String joinTable;
        private String prefix;
        private String joinId;

        public TableSearchColumn() {
        }

        public String getColumn() {
            return this.column;
        }

        public void setColumn(String column) {
            this.column = column;
        }

        public String getJoinTable() {
            return this.joinTable;
        }

        public void setJoinTable(String joinTable) {
            this.joinTable = joinTable;
        }

        public String getPrefix() {
            return this.prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }

        public String getJoinId() {
            return this.joinId;
        }

        public void setJoinId(String joinId) {
            this.joinId = joinId;
        }
    }
}
