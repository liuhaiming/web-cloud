package com.web.cloud.utils;

/**
 * Created by liuhaiming on 2015/9/5.
 */
public class CommonConsts {
    /** 人员头像根目录 */
    public static final String HEADER_IMAGE_PATH = "static/head_img/employee_img";

    /** 默认头像 */
    public static final String DEFAULT_HEADER_IMAGE_PATH = "static/img/user-default-header.png";

    /** session attribute key --- 登陆人员 */
    public static final String SESSION_ATTR_KEY_LOGIN_USER = "loginUser";

    /** session attribute key --- 登陆标识 */
    public static final String SESSION_ATTR_KEY_IS_LOGIN = "isLogin";

    /** session attribute key --- 用户级别（0、系统管理员；1、组织管理员； 2、业务用户） */
    public static final String SESSION_ATTR_KEY_USER_TYPE = "userType";

    /** session attribute key --- 权限功能列表 */
    public static final String SESSION_ATTR_KEY_PERMISSION_FUNCS = "funcList";

    /** session attribute key --- 权限旅行社列表 */
    public static final String SESSION_ATTR_KEY_PERMISSION_TRAVEL = "travelList";

    /** session attribute key --- 商户信息 */
    public static final String SESSION_ATTR_KEY_STORE = "store";

    /** session attribute key --- 销售商信息 */
    public static final String SESSION_ATTR_KEY_AGENT = "agent";

    /** USER TYPE -- SUPER ADMIN */
    public static final int USER_TYPE_SYSTEM_ADMIN = 0;

    /** USER TYPE -- ORG ADMIN */
    public static final int USER_TYPE_ORG_ADMIN = 1;

    /** USER TYPE -- BUSINESS */
    public static final int USER_TYPE_BUSI = 2;

    public static final String BOOLEAN_TRUE = "Y";

    public static final String BOOLEAN_FALSE = "N";

    public static final int FUNC_TYPE_MODULE = 0;
    public static final int FUNC_TYPE_LEAF = 1;
}
