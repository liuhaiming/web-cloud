package com.web.cloud.utils;

import com.web.cloud.model.pub.OrgInfo;

/**
 * 组织信息Holder
 * Created by hubo on 16/4/1
 */
public class OrgInfoHolder {

    private static final ThreadLocal<OrgInfo> threadLocal = new ThreadLocal<>();

    public static OrgInfo getOrgInfo() {
        return threadLocal.get();
    }

    public static String getOrgId() {
        return threadLocal.get().getId();
    }

    public static void setOrgInfo(OrgInfo orgInfo) {
        threadLocal.set(orgInfo);
    }

    public static void clear() {
        threadLocal.set(null);
    }

}
