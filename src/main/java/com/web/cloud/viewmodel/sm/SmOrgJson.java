package com.web.cloud.viewmodel.sm;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.web.cloud.model.sm.SmOrg;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuhaiming on 2015/8/11.
 */
public class SmOrgJson {
    public static final String ICON_LEAF = "fa fa-file-o icon-state-warning";

    private String id;

    private String text;

    private String icon;

    private String appId;

    private String appSecret;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Long ts;

    private Map<String, Object> state = new HashMap<>();

    private List<SmOrgJson> children;

    public SmOrgJson(SmOrg org) {
        text = org.getCode() + " " + org.getName();
        id = org.getId();
        ts = org.getTs();
        appId = org.getAppId();
        appSecret = org.getAppSecret();
        state.put("opened", true);
    }

    public SmOrgJson(String orgText) {
        text = orgText;
        id = "root";
        state.put("opened", true);
    }

    public List<SmOrgJson> getChildren() {
        return children;
    }

    public void setChildren(List<SmOrgJson> children) {
        this.children = children;
    }

    public Map<String, Object> getState() {
        return state;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }
}
