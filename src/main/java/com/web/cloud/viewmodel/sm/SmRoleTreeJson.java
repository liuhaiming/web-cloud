package com.web.cloud.viewmodel.sm;

import com.web.cloud.model.sm.SmRole;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuhaiming on 2015/8/11.
 */
public class SmRoleTreeJson {
    public static final String ICON_LEAF = "fa fa-file icon-state-warning";

    private String id;

    private String text;

    private String icon;

    private Map<String, Object> state = new HashMap<>();

    private List<SmRoleTreeJson> children;

    public SmRoleTreeJson(SmRole role) {
        text = role.getCode() + " " + role.getName();
        id = role.getId();
        state.put("opened", true);
    }

    public SmRoleTreeJson(String orgText) {
        text = orgText;
        id = "root";
        state.put("opened", true);
    }

    public List<SmRoleTreeJson> getChildren() {
        return children;
    }

    public void setChildren(List<SmRoleTreeJson> children) {
        this.children = children;
    }

    public Map<String, Object> getState() {
        return state;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
