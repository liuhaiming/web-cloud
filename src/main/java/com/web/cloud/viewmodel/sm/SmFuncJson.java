package com.web.cloud.viewmodel.sm;



import com.web.cloud.model.pub.tree.ITreeNode;
import com.web.cloud.model.sm.SmFunc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 功能节点DTO
 * Created by hubo on 2015/8/10
 */
public class SmFuncJson extends SmFunc implements ITreeNode, Serializable{

    public SmFuncJson(SmFunc func) {
        setId(func.getId());
        setCode(func.getCode());
        setName(func.getName());
        setIncode(func.getIncode());
        setIcon(func.getIcon());
        setHref(func.getHref());
        setParentId(func.getParentId());
        setStatus(func.getStatus());
        setTs(func.getTs());
        setDr(func.getDr());
        setSq(func.getSq());
        setNodeType(func.getNodeType());
    }

    public SmFuncJson() {

    }

    private boolean start = false;

    private List<SmFuncJson> children = new ArrayList<>();

    public boolean isStart() {
        return start;
    }

    public void setStart(boolean start) {
        this.start = start;
    }

    public List<SmFuncJson> getChildren() {
        return children;
    }

    @Override
    public String getNodeId() {
        return getId();
    }

    public void addChildFunc(SmFuncJson child) {
        children.add(child);
    }
}
