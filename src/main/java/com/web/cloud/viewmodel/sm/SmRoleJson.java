package com.web.cloud.viewmodel.sm;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.web.cloud.model.sm.SmRole;

/**
 * Created by liuhaiming on 2015/8/14.
 */
public class SmRoleJson {

    @JsonProperty(value = "DT_RowId", index = 0)
    private String id;

    private String checked = "<input type='checkbox' class='checkboxes' value='1'/>";

    private int index;

    private String orgId;

    private String orgName;

    private String code;

    private String name;

    private int status;

    private String statusName;

    private String memo;

    private long ts;

    public SmRoleJson(int index, SmRole role) {
        this.index = index;
        this.id = role.getId();
        this.orgId = role.getOrgId();
        this.orgName = (role.getOrg() == null ? "" : role.getOrg().getName());

        this.code = role.getCode();
        this.name = role.getName();
        this.status = role.getStatus();
        this.memo = role.getMemo();
        this.ts = role.getTs();

        if(status == 0) {
            statusName = "启用";
        } else  if(status == 1){
            statusName = "禁用";
        }
    }

    public SmRoleJson() {
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
