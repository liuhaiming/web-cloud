package com.web.cloud.i18n;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Created by liuhaiming on 2016-05-29.
 */
@Component
public final class MessageUtils implements ApplicationContextAware {
    private static ResourceBundleMessageSource messageSource;

    public MessageUtils() {
    }

    public static String getMessage(String code) {
        return messageSource.getMessage(code, (Object[])null, "", Locale.getDefault());
    }

    public static String getMessage(String code, String defaultMessage) {
        return messageSource.getMessage(code, (Object[])null, defaultMessage, Locale.getDefault());
    }

    public static String getMessage(String code, Object[] args) {
        return messageSource.getMessage(code, args, "", Locale.getDefault());
    }

    public static String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
        return messageSource.getMessage(code, args, defaultMessage, locale);
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        messageSource = (ResourceBundleMessageSource)applicationContext.getBean("messageSource");
    }
}
