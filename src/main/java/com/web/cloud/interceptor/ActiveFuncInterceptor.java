package com.web.cloud.interceptor;

import com.web.cloud.constances.GlobalVariableConsts;
import com.web.cloud.constances.SessionConsts;
import com.web.cloud.model.pub.tree.TreeNode;
import com.web.cloud.model.sm.SmFunc;
import com.web.cloud.utils.RequestUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Stack;

/**
 * 当登陆用户点击某个菜单时，查询完整的菜单路径，保存到session中，从而实现面包屑功能封装
 * 
 * @author 马兆永
 *
 */
public class ActiveFuncInterceptor implements HandlerInterceptor {

	/**
	 *
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		return true;
	}

	/**
	 * 从请求后获取地址中带的code，根据code判断
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// 判断当前请求是否为登陆用户，不是登陆用户则不处理   如果是ajax请求也不做处理
		Boolean isLogin = (Boolean) request.getSession().getAttribute(SessionConsts.SESSION_ATTRIBUTE_IS_LOGIN);
		isLogin = (isLogin == null ? false : isLogin);

		boolean isAjax = RequestUtil.isAjax(request);
		if( !( isLogin || isAjax ) ){
			return;
		}

		HttpSession session = request.getSession();
		String code = request.getParameter(GlobalVariableConsts.ACTIVE_FUNC_CODE);
		if(StringUtils.isEmpty(code)) {
//			return;
			code = "INDEX";
		}

		TreeNode<SmFunc> root =  (TreeNode<SmFunc>) session.getAttribute(SessionConsts.SESSION_ATTRIBUTE_FUNC_LIST);
		if(root == null) {
			return;
		}

		TreeNode<SmFunc> selectedNode = getSelectedFunc(code, root);
		if(selectedNode != null){
			session.setAttribute(SessionConsts.SESSION_ATTRIBUTE_ACTIVE_FUNCS, getFullPath(selectedNode));
            session.setAttribute(SessionConsts.SESSION_ATTRIBUTE_ACTIVE_FUNC_NAME, selectedNode.getValue().getName());
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
	
	/**
	 * 获取当前用户选择的
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private TreeNode<SmFunc> getSelectedFunc(String code,TreeNode<SmFunc> root){
		if(root.getValue() != null ){
			String vcode = root.getValue().getCode();
			if(!StringUtils.isEmpty(vcode) && vcode.equalsIgnoreCase(code)){
				return root;
			}
		}
		for(TreeNode<SmFunc> node : root.getChilds()){
			 TreeNode<SmFunc> ret = getSelectedFunc(code,node);
			 if(ret!=null){
				 return ret;
			 }
		}
		return null;
	}
	
	private SmFunc[] getFullPath(TreeNode<SmFunc> node){
		SmFunc[] result;
		final Stack<SmFunc> stack = new Stack<>();
		getFullPath(node,stack);
		SmFunc[] funcs = new SmFunc[stack.size()];
		for(int n=stack.size(), i=n-1;i>=0;i--){
			funcs[n-1-i] = stack.get(i);
		}
		return funcs;
	}

	private void getFullPath(TreeNode<SmFunc> node,final Stack<SmFunc> stack){
		if(node==null||node.getValue()==null)return;
		stack.push(node.getValue());
		getFullPath(node.getParentNode(),stack);
	}
}
