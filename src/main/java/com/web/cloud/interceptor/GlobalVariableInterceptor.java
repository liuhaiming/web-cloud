package com.web.cloud.interceptor;

import com.web.cloud.constances.GlobalVariableConsts;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 全局变量处理类
 *
 * @author liuhaiming
 */
public class GlobalVariableInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        String root = request.getContextPath();
        root = ( root.endsWith("/") ? root.substring(0, root.length() - 1) : root );

        request.setAttribute(GlobalVariableConsts.ROOT_PATH, root);

        request.setAttribute(GlobalVariableConsts.PAGE_PATH, root + "/assets/page");
        request.setAttribute(GlobalVariableConsts.ADMIN_LTE_PATH, root + "/assets/dist/bower_components/AdminLTE");
        request.setAttribute(GlobalVariableConsts.COMPONENT_PATH, root + "/assets/dist/bower_components");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
