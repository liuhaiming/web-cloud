package com.web.cloud.beetls;

import org.apache.log4j.Logger;
import org.beetl.core.Tag;
import org.beetl.core.Template;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

/**
 * Created by liuhaiming on 2015/12/25.
 */
@Service("customTag")
@Scope("prototype")
public class CustomTag extends Tag {

    private static final Logger LOG = Logger.getLogger(CustomTag.class);

    @Override
    public void render() {
        Map attrs = (Map) args[1];
        //文件路径
        String path = (String) attrs.get("path");

        String prefix = gt.getConf().getResourceMap().get("tagRoot");
        String suffix = gt.getConf().getResourceMap().get("tagSuffix");
        Template tpl = gt.getTemplate(prefix+"/"+path+"."+suffix);
//        tpl.binding(attrs);
        try {
            this.ctx.byteWriter.writeString(tpl.render());
        } catch (IOException e) {
            LOG.error(e);
        }
    }

}
