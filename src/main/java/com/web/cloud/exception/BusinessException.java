package com.web.cloud.exception;

import com.web.cloud.i18n.MessageUtils;

/**
 * 业务异常
 * Created by liuhaiming on 2016/5/1
 */
public class BusinessException extends Exception{
    private String code;

    public BusinessException(String code) {
        super(MessageUtils.getMessage(code));
        this.code = code;
    }

    public BusinessException(String code, Object[] args) {
        super(MessageUtils.getMessage(code, args));
        this.code = code;
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException() {
    }

    public String getCode() {
        return this.code;
    }
}
