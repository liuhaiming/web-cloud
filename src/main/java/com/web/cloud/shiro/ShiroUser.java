package com.web.cloud.shiro;


import com.web.cloud.model.sm.SmUser;

import java.io.Serializable;

/**
 * Created by liuhaiming on 2015/12/10.
 */
public class ShiroUser implements Serializable {
    private SmUser smUser = null;

    public SmUser getSmUser() {
        return smUser;
    }

    public void setUser(SmUser smUser) {
        this.smUser = smUser;
    }

    public ShiroUser(SmUser smUser) {
        this.smUser = smUser;
    }

    @Override
    public int hashCode() {
        return smUser == null ? this.hashCode() : (smUser.getCode() == null ? smUser.hashCode() : smUser.getCode().hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        if ( obj == null || this.smUser == null ) {
            return false;
        }

        if ( obj.getClass() != this.smUser.getClass() ) {
            return false;
        }

        if (smUser == this.smUser) {
            return true;
        }

        SmUser smUser = (SmUser) obj;
        return this.getSmUser().getCode().equals(smUser.getCode());
    }
}
