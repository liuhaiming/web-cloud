package com.web.cloud.shiro;

import com.web.cloud.utils.EncryptUtil;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

/**
 * 登陆密码校验器
 * Created by liuhaiming on 2015/12/17.
 */
public class CredentialsMatcher extends SimpleCredentialsMatcher {
    @Override
    public boolean doCredentialsMatch(AuthenticationToken authcToken, AuthenticationInfo info) {
        UsernamePasswordToken token = (UsernamePasswordToken) authcToken;

        // the password in DB
        String accountCredentials = (String) getCredentials(info);

        // the username and password from login page
        String username = String.valueOf(token.getPrincipal());
        String password = new String((char[]) token.getCredentials());
        String encryptPsw = EncryptUtil.encryptForUser(username, password);

        // check the password is same
        return accountCredentials.equals(encryptPsw);
    }
}
