package com.web.cloud.constances;

/**
 * Created by liuhaiming on 2015/12/12.
 */
public interface DBConsts {
    /** DB标准字段-主键 */
    public static final String FIELD_ID = "id";

    /** DB标准字段-状态 */
    public static final String FIELD_STATUS = "status";

    /** DB标准字段-逻辑状态 */
    public static final String FIELD_DR = "dr";

    /** DB标准字段-时间戳 */
    public static final String FIELD_TS = "ts";

    /** 数据逻辑状态 - 正常态 */
    public static final Short DR_NORMAL = 0;

    /** 数据逻辑状态 - 删除态 */
    public static final Short DR_DELETE = 1;

    /** 启用标识 - 启用 */
    public static final Short STATUS_ENABLE = 0;

    /** 启用标识 - 停用 */
    public static final Short STATUS_DISABLE = 1;

    /** 性别 - 男 */
    public static final Short SEX_MALE = 0;

    /** 启用标识 - 女 */
    public static final Short SEX_FEMALE = 1;

    /** 节点类型 - 虚拟节点 */
    public static final Short NODE_TYPE_DUMMY = 0;

    /** 节点类型 - 功能节点 */
    public static final Short NODE_TYPE_FUNC = 1;
}
