package com.web.cloud.constances;

/**
 * Created by liuhaiming on 2016-05-04.
 */
public interface GlobalVariableConsts {
    public static final String ROOT_PATH = "root";

    public static final String PAGE_PATH = "page";

    public static final String ADMIN_LTE_PATH = "lte";

    public static final String COMPONENT_PATH = "dist";

    /**
     * http response 返回json时的头部
     */
    public static final String CONTENT_TYPE_JSON = "application/json;charset=UTF-8";
    /**
     * http response 返回json时的头部 ,只针对IE
     */
    public static final String CONTENT_TYPE_IE = "text/plain;charset=UTF-8";

    public static final String ACTIVE_FUNC_CODE = "activeFuncCode";
}
