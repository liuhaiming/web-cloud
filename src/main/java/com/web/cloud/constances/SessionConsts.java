package com.web.cloud.constances;

/**
 * Created by liuhaiming on 2016-04-30.
 */
public interface SessionConsts {
    public static final String SESSION_ATTRIBUTE_IS_LOGIN = "isLogin";

    public static final String SESSION_ATTRIBUTE_LOGIN_USER = "loginUser";

    public static final String SESSION_ATTRIBUTE_USER_TYPE = "userType";

    public static final String SESSION_ATTRIBUTE_FUNC_LIST = "funcList";

    public static final String SESSION_ATTRIBUTE_ACTIVE_FUNCS = "activeFuncs";

    public static final String SESSION_ATTRIBUTE_ACTIVE_FUNC_NAME = "activeFuncName";
}
