package com.web.cloud.dao.sm;

import com.web.cloud.base.dao.BaseDao;
import com.web.cloud.model.sm.SmOrg;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SmOrgDao extends BaseDao<SmOrg> {

    List<SmOrg> selectByIds(@Param("orgIds") String[] orgIds);

    List<SmOrg> selectEnableData();

    int selectByInnerCode(String incode);

    List<SmOrg> selectEnableOrgsByIncode(@Param("incode") String incode);

    List<SmOrg> selectOrgsByIncode(@Param("incode") String incode);

}