package com.web.cloud.dao.sm;

import com.web.cloud.base.dao.BaseDao;
import com.web.cloud.model.sm.SmUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户Dao接口
 * 
 * @author liuhm
 * @since 2015年12月16日
 **/
@Repository
public interface SmUserDao extends BaseDao<SmUser> {
    /**
     * 根据用户名查用户信息
     * @param loginName
     * @return
     */
    SmUser selectByLoginName(@Param("loginName") String loginName);

    int updatePasswordByPrimaryKey(@Param("id") String id, @Param("newPassword") String newPassword);

    List<SmUser> selectRefUserListLike(@Param("searchText") String searchText, @Param("orgId") String orgId);

    List<SmUser> selectRefUserList(@Param("orgId") String orgId);

}