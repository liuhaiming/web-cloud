package com.web.cloud.dao.sm;

import com.web.cloud.base.dao.BaseDao;
import com.web.cloud.model.sm.SmFunc;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户Dao接口
 * 
 * @author liuhm
 * @since 2015年12月16日
 **/
@Repository
public interface SmFuncDao extends BaseDao<SmFunc> {
    /**
     * 查询全部功能节点
     * @return
     */
    List<SmFunc> selectAllFuncs();

    List<SmFunc> selectPermissionFuncs(@Param("roleIds") List<String> roleIds);
}