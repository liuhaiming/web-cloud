package com.web.cloud.dao.sm;

import com.web.cloud.base.dao.BaseDao;
import com.web.cloud.model.sm.SmRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SmRoleDao extends BaseDao<SmRole> {

    List<SmRole> selectByIds(@Param("roleIds") String[] roleIds);

    List<SmRole> selectStatusEnableAdmin();

    List<SmRole> selectStatusEnable(@Param("orgId") String orgId);
}
