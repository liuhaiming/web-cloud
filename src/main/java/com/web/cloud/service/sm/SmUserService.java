package com.web.cloud.service.sm;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.web.cloud.constances.DBConsts;
import com.web.cloud.constances.ModelConsts;
import com.web.cloud.dao.sm.SmUserDao;
import com.web.cloud.model.sm.SmRole;
import com.web.cloud.model.sm.SmUser;
import com.web.cloud.service.pub.BaseService;
import com.web.cloud.service.pub.OrgBaseService;
import com.web.cloud.utils.CommonConsts;
import com.web.cloud.wapper.sm.SmUserDataTableQueryArgWrapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户Service实现类
 *
 * @author liuhm
 * @since 2015年12月12日
 */
@Service
public class SmUserService extends OrgBaseService<SmUser> {
    private static final Logger log = LoggerFactory.getLogger(SmUserService.class);

    @Autowired
    private SmRoleService roleSerrvice;

    @Autowired
    private SmOrgService orgService;

    public SmUserDao getDao() {
        return (SmUserDao) this.dao;
    }

    public SmUser selectUserByCode(String code) {
        SmUser user = getDao().selectByLoginName(code);
        if (user != null) {
            user.setBusiOrg(user.getBusiOrgId() == null ? null : orgService.get(user.getBusiOrgId()) );
            user.setRole( user.getRoleId() == null ? null : roleSerrvice.get(user.getRoleId()) );
        }

        return user;
    }

    /**
     * 修改密码
     * @param id 员工ID
     * @param newPassword 新的密码
     */
    public void modifyPassword(String id, String newPassword) {
        getDao().updatePasswordByPrimaryKey(id, newPassword);
    }


    /**
     * 根据类型获取员工角色
     *
     * @param type
     * 0: 主角色
     * 1：辅助角色
     * 2：主、辅角色
     *
     * @return
     */
    public List<SmRole> findUserRoles(String userId, Short type) {
        SmUser user = get(userId);
        List<SmRole> roleLst = null;

        if ( type == SmUser.ROLE_FIND_TYPE_MAIN ) {
            roleLst = findMainRole(user);
        } else if ( type == SmUser.ROLE_FIND_TYPE_AUXILIARY ) {
//            roleLst = findAuxiliaryRole(user);
        } else if ( type == SmUser.ROLE_FIND_TYPE_ALL ) {
            roleLst = findAllRole(user);
        }

        return roleLst;
    }

    public List<SmRole> findAllRole(SmUser employee) {
        List<SmRole> roleLst = new ArrayList<>();
        roleLst.addAll( findMainRole(employee) );
//        roleLst.addAll( findAuxiliaryRole(employee) );

        return roleLst;
    }

    public List<SmRole> findMainRole(SmUser employee) {
        List<SmRole> roleLst = new ArrayList<>();
        SmRole role = roleSerrvice.get(employee.getRoleId());
        if (role != null) {
            roleLst.add( role );
        }

        return roleLst;
    }
}
