package com.web.cloud.service.sm;

import com.web.cloud.constances.ModelConsts;
import com.web.cloud.dao.sm.SmFuncDao;
import com.web.cloud.dao.sm.SmUserDao;
import com.web.cloud.model.pub.tree.TreeNode;
import com.web.cloud.model.sm.SmFunc;
import com.web.cloud.model.sm.SmRole;
import com.web.cloud.model.sm.SmUser;
import com.web.cloud.service.pub.BaseService;
import com.web.cloud.utils.CommonConsts;
import com.web.cloud.utils.TreeUtil;
import com.web.cloud.viewmodel.sm.SmFuncJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户Service实现类
 *
 * @author liuhaiming
 * @since 2016年05月16日
 */
@Service("funcService")
public class SmFuncService extends BaseService<SmFunc> {
    private static Logger log = LoggerFactory.getLogger(SmFuncService.class);

    @Autowired
    private SmUserService userService;

    private SmFuncDao getDao() {
        return (SmFuncDao) this.dao;
    }

    public List<SmFunc> findAllFunc() {
        return getDao().selectAll();
    }

    private TreeNode<SmFuncJson> getFuncTreeList(List<SmFunc> funcList) {
        log.info("PermissionFunc Func list : ", funcList);

        List<SmFuncJson> funcDtoList = new ArrayList<>(funcList.size());


//        List<SmFuncJson> result = new ArrayList<>();

        for (SmFunc func : funcList) {
            SmFuncJson funcDto = new SmFuncJson(func);
            funcDtoList.add(funcDto);
        }

        TreeNode<SmFuncJson> root = TreeUtil.buildTree(funcDtoList);

//        for (TreeNode<SmFuncJson> node : root.getChilds()) {
//            SmFuncJson func = node.getValue();
//            result.add(func);
//            for (TreeNode<SmFuncJson> childNode : node.getChilds()) {
//                func.addChildFunc(childNode.getValue());
//            }
//        }
//
//        log.info("Func Tree list : ", result);

        return root;
    }

    public TreeNode<SmFuncJson> getFuncTreeList() {
        List<SmFunc> funcList = findAllFunc();
        return getFuncTreeList(funcList);
    }


    public TreeNode<SmFuncJson> getPermissionFuncTree(String userId, int userType) {
        if ( userType == CommonConsts.USER_TYPE_ORG_ADMIN ) {
            return getFuncTreeList();
        }

        List<SmRole> roleLst = userService.findUserRoles(userId, SmUser.ROLE_FIND_TYPE_ALL);
        if (roleLst.size() == 0) {
            return null;
        }

        List<String> roleIds = new ArrayList<>();
        for (SmRole role : roleLst) {
            roleIds.add(role.getId());
        }

        List<SmFunc> funcList = getPermissionFuncs(roleIds);
        TreeNode<SmFuncJson> permissionRoot = getFuncTreeList(funcList);
        removeBlankModule(permissionRoot);

        return permissionRoot;
    }

    private void removeBlankModule(TreeNode<SmFuncJson> node) {
        if ( node.getChilds() != null && node.getChilds().size() > 0 ) {
            for ( TreeNode<SmFuncJson> child : node.getChilds() ) {
                removeBlankModule(child);

            }
        }

        if ( node.getValue() != null && node.getValue().getNodeType() == CommonConsts.FUNC_TYPE_MODULE && (node.getChilds() == null || node.getChilds().size() == 0) ) {
            node.getParentNode().getChilds().remove(node);
        }

    }

    public List<SmFunc> getPermissionFuncs(List<String> roleIds) {
        return getDao().selectPermissionFuncs(roleIds);
    }

}
