package com.web.cloud.base.dao;

import com.web.cloud.base.dao.mapper.BatchLogicDeleteMapper;
import com.web.cloud.base.dao.mapper.CommonSelectMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

/**
 * Created by liuhaiming on 2016-04-28.
 */
public interface BaseDao<T> extends Mapper<T>, InsertListMapper<T>, BatchLogicDeleteMapper<T>, CommonSelectMapper<T> {
}
