package com.web.cloud.base.dao.mapper;

import com.web.cloud.base.dao.provider.BatchLogicDeleteMapperProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import java.util.List;

/**
 * Created by liuhaiming on 2015/12/28
 */
public interface BatchLogicDeleteMapper<T> {

    @UpdateProvider(type = BatchLogicDeleteMapperProvider.class, method = "dynamicSQL")
    int batchDelete(List<T> arr);

}
