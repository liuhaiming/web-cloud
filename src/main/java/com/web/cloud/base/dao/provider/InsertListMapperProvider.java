package com.web.cloud.base.dao.provider;

import org.apache.ibatis.mapping.MappedStatement;
import tk.mybatis.mapper.entity.EntityColumn;
import tk.mybatis.mapper.entity.EntityTable;
import tk.mybatis.mapper.mapperhelper.EntityHelper;
import tk.mybatis.mapper.mapperhelper.MapperHelper;
import tk.mybatis.mapper.mapperhelper.MapperTemplate;

import java.util.Iterator;

/**
 * Created by liuhaiming on 2016/1/29.
 */
public class InsertListMapperProvider extends MapperTemplate {
    public InsertListMapperProvider(Class<?> mapperClass, MapperHelper mapperHelper) {
        super(mapperClass, mapperHelper);
    }

    public String insertList(MappedStatement ms) {
        Class entityClass = this.getEntityClass(ms);
        EntityTable table = EntityHelper.getEntityTable(entityClass);
        StringBuilder sql = new StringBuilder();
        sql.append("insert into ");
        sql.append(table.getName());
        sql.append("(");
        boolean first = true;

        Iterator var6;
        EntityColumn column;
        for(var6 = table.getEntityClassColumns().iterator(); var6.hasNext(); first = false) {
            column = (EntityColumn)var6.next();
            if(!first) {
                sql.append(",");
            }

            sql.append(column.getColumn());
        }

        sql.append(") values ");
        sql.append("<foreach collection=\"list\" item=\"record\" separator=\",\" >");
        sql.append("(");
        first = true;

        for(var6 = table.getEntityClassColumns().iterator(); var6.hasNext(); first = false) {
            column = (EntityColumn)var6.next();
            if(!first) {
                sql.append(",");
            }

            sql.append("#{record.").append(column.getProperty()).append("}");
        }

        sql.append(")");
        sql.append("</foreach>");
        return sql.toString();
    }
}
