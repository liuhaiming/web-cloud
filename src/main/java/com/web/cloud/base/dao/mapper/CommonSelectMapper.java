package com.web.cloud.base.dao.mapper;

import com.web.cloud.base.dao.provider.BatchLogicDeleteMapperProvider;
import com.web.cloud.base.dao.provider.CommonSelectMapperProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * Created by liuhaiming on 2016/01/08
 */
public interface CommonSelectMapper<T> {
    @SelectProvider(
            type = CommonSelectMapperProvider.class,
            method = "dynamicSQL"
    )
    List<T> selectLike(@Param("searchValue") String var1, @Param("record") T var2);

    @SelectProvider(
            type = CommonSelectMapperProvider.class,
            method = "dynamicSQL"
    )
    List<T> selectByIdList(List<String> var1);

}

