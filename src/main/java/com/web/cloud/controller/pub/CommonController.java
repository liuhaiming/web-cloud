package com.web.cloud.controller.pub;

import com.web.cloud.constances.SessionConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * 公共视图控制器
 * 
 * @author luihm
 * @since 2014年4月15日 下午4:16:34
 **/
@Controller
public class CommonController {
    @Autowired
    private HttpSession session;

    /**
     * 登出
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout() {
        session.removeAttribute(SessionConsts.SESSION_ATTRIBUTE_IS_LOGIN);
        session.removeAttribute(SessionConsts.SESSION_ATTRIBUTE_LOGIN_USER);

        return "login";
    }

    /**
     * 登出
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "pub/login";
    }

    /**
     * 首页
     * @param model
     * @return
     */
    @RequestMapping("/")
    public String index(Model model) {
        return "pub/index";
    }

}