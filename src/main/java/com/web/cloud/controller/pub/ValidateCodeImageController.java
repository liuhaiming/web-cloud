package com.web.cloud.controller.pub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * Created by liuhaiming on 2015/12/18
 */
@Controller
public class ValidateCodeImageController {

    private static Logger log = LoggerFactory.getLogger(ValidateCodeImageController.class);

    public static final String VALIDATE_CODE = "VALIDATE_CODE";
    private final Random random = new Random();
    private final static String randString = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private final int width = 100;//图片宽度
    private final int height = 32;//图片高度
    private int lineSize = 30;//线条个数
    private int stringNum = 4;//个数
    /**
     * 获取字体
     */
    private Font getFont(){
        return new Font("Fixedsys",Font.BOLD, 22);
    }
    /**
     * 获取随机颜色
     */
    private Color getRandColor(){
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);

        return new Color(r,g,b);
    }

    @RequestMapping("/img/validate_code")
    public void getRandCode(HttpServletRequest request,
                            HttpServletResponse response) {

        response.setContentType("image/jpeg");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expire", 0);

        HttpSession session = request.getSession();
        BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_BGR);
        Graphics g = image.getGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, width, height);
        g.setFont(new Font("Times New Roman",Font.ROMAN_BASELINE,18));
        g.setColor(getRandColor());
        //画线条
        for(int i=0;i<=lineSize;i++){
            drowLine(g);
        }

        String randomString = "";
        for(int i = 1; i <= stringNum; i++){
            randomString = drowString(g, randomString, i);
        }
        session.removeAttribute(VALIDATE_CODE);
        session.setAttribute(VALIDATE_CODE, randomString);

        g.dispose();

        try {
            ImageIO.write(image, "JPEG", response.getOutputStream());
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
    }

    /**
     * 画字符
     */
    private String drowString(Graphics g,String randomString,int i){
        g.setFont(getFont());
        g.setColor(new Color(random.nextInt(101),random.nextInt(111),random.nextInt(121)));
        String rand = String.valueOf(getRandomString(random.nextInt(randString.length())));
        randomString +=rand;
        g.translate(random.nextInt(3), random.nextInt(3));
        g.drawString(rand, 14 * i, 20);
        return randomString;
    }

    /**
     * 画随机线条，防止破解图片内容
     */
    private void drowLine(Graphics g) {
        int x = random.nextInt(width);
        int y = random.nextInt(height);
        int xl = random.nextInt(15);
        int yl = random.nextInt(15);
        g.drawLine(x, y, x+xl, y+yl);
    }

    /**
     * 获取随机字符
     */
    public String getRandomString(int num){
        return String.valueOf(randString.charAt(num));
    }


}
