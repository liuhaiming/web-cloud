package com.web.cloud.controller.sm;

import com.web.cloud.constances.DBConsts;
import com.web.cloud.exception.BusinessException;
import com.web.cloud.model.sm.SmOrg;
import com.web.cloud.service.sm.SmOrgService;
import com.web.cloud.viewmodel.sm.SmOrgJson;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuhaiming on 2016-05-29.
 */
@Controller
@RequestMapping("sm")
public class SmOrgController {
    private static final Logger log = LoggerFactory.getLogger(SmOrgController.class);

    @Autowired
    private SmOrgService orgService;

    @RequestMapping("/sm_org")
    public String index() {
        return "sm/sm_org";
    }

    @ModelAttribute
    public void getOrg(@RequestParam(value = "id", required = false) String id, Map<String, Object> map) {
        if( StringUtils.isNotEmpty(id) ) {
            map.put( "org", orgService.get(id) );
            map.put( "smOrg", orgService.get(id) );
        }
    }

    @RequestMapping("/sm_org_tree")
    @ResponseBody
    public List<SmOrgJson> getOrgTree() {
        return getOrgTree(false);
    }

    @RequestMapping("/sm_org_tree_enable")
    @ResponseBody
    public List<SmOrgJson> getEnablergTree() {
        return getOrgTree(true);
    }

    private List<SmOrgJson> getOrgTree(boolean enable) {
        List<SmOrgJson> data = new ArrayList<>();

        List<SmOrgJson> children = orgService.getOrgTreeRoot(enable);
        SmOrgJson root = new SmOrgJson("组织结构");
        root.setChildren(children);

        data.add(root);
        return data;
    }

    @RequestMapping(value = "/sm_org/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> get(@PathVariable("id") String id) {
        Map<String, Object> result = new HashMap<>();
        result.put("result", "success");
        result.put("data", orgService.get(id));
        return result;
    }

    @RequestMapping("/edit")
    public
    @ResponseBody
    Map<String, Object> edit(String id) {
        Map<String, Object> orgData = new HashMap<>();

        orgData.put("org", orgService.get(id));
        orgData.put("success", "true");
        return orgData;
    }

    @RequestMapping(value = "/sm_org", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> save(SmOrg org, Errors errors, Map<String, Object> map) {
        Map<String, Object> result = new HashMap<>();

        if(errors.getErrorCount() > 0) {
            result.put("result", "error");
            for (FieldError err : errors.getFieldErrors()) {
                map.put("err_" + err.getField(), err.getDefaultMessage());
            }
            return result;
        }

        try {
            // 设置内部编码
            String incode = orgService.createInncode(org.getParentId());

            org.setIncode(incode);
            orgService.saveOrgAndCreateDefaultRole(org);
            result.put("result", "success");
        } catch (BusinessException e) {
            log.error(e.getMessage(), e);
            result.put("result", "error");
        }

        return result;
    }

    @RequestMapping(value = "/sm_org", method = RequestMethod.PUT)
    @ResponseBody
    public Map<String, Object> update(SmOrg org, Errors errors, Map<String, Object> map) {

        Map<String, Object> result = new HashMap<>();

        if(errors.getErrorCount() > 0) {
            result.put("result", "error");
            for (FieldError err : errors.getFieldErrors()) {
                map.put("err_" + err.getField(), err.getDefaultMessage());
            }
            return result;
        }

        try {
            orgService.update(org);
            result.put("result", "success");
        } catch (BusinessException e) {
            log.error(e.getMessage(), e);
            result.put("result", "error");
            result.put("errorMsg", e.getMessage());
        }

        return result;
    }

    @RequestMapping(value = "/sm_org", method = RequestMethod.DELETE)
    @ResponseBody
    public Map<String, Object> delete(SmOrg org) {

        Map<String, Object> result = new HashMap<>();

        try {
            orgService.delete(org);
            result.put("result", "success");
        } catch (BusinessException e) {
            log.error(e.getMessage(), e);
            result.put("result", "error");
            result.put("errorMsg", e.getMessage());
        }

        return result;
    }

    @RequestMapping(value = "/sm_org/status/{status}", method = RequestMethod.PUT)
    @ResponseBody
    public Map<String, Object> updateStatus(@PathVariable Short status, SmOrg org ) {
        Map<String, Object> result = new HashMap<>();
        org.setStatus(status);

        try {
            if(status == DBConsts.STATUS_ENABLE) {
                orgService.enable(org);
            }else {
                orgService.disable(org);
            }
            result.put("result", "success");
        } catch (BusinessException e) {
            log.error(e.getMessage(), e);
            result.put("result", "error");
            result.put("errorMsg", e.getMessage());
        }

        return result;
    }
}